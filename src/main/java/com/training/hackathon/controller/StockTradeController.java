package com.training.hackathon.controller;

import com.training.hackathon.entity.StockTrade;
import com.training.hackathon.service.StockTradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/stocktrade")
public class StockTradeController {

    @Autowired
    StockTradeService stockTradeService;

    @GetMapping
    public List<StockTrade> findAll() {
        return stockTradeService.findAll();
    }

    @GetMapping("/user/{userId}")
    public List<StockTrade> findByUserId(@PathVariable Long userId) {
        return stockTradeService.findByUserId(userId);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StockTrade> findById(@PathVariable Long id) {
        try {
            return new ResponseEntity<StockTrade>(stockTradeService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<StockTrade>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/stockTicker/{stockTicker}")
    public List<StockTrade> findByStockTicker(@PathVariable String stockTicker) {
        return stockTradeService.findByStockTicker(stockTicker);

    }

    @GetMapping("/statusCode/{statusCode}")
    public List<StockTrade> findByStatusCode(@PathVariable Long statusCode) {
        return stockTradeService.findByStatusCode(statusCode);
    }

    @PostMapping
    public ResponseEntity<StockTrade> save(@RequestBody StockTrade stockTrade) {
        return new ResponseEntity<StockTrade>(stockTradeService.save(stockTrade), HttpStatus.CREATED);
    }
}
