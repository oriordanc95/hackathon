package com.training.hackathon.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {
    private User user;
    private static final Long testId = 1L;
    private static final String testFirstName = "Seto";
    private static final String testLastName = "Kaiba";

    @BeforeEach
    public void setup() {
        user = new User();
    }

    @Test
    public void testSetId() {
        user.setId(testId);
        assertEquals(testId, user.getId());
    }

    @Test
    public void testSetFirstName() {
        user.setFirstName(testFirstName);
        assertEquals(testFirstName, user.getFirstName());
    }

    @Test
    public void testSetLastName() {
        user.setLastName(testLastName);
        assertEquals(testLastName, user.getLastName());
    }

    @Test
    public void testToString() {
        String expected = "User{" +
                "id=" + testId +
                ", firstName='" + testFirstName + '\'' +
                ", lastName='" + testLastName + '\'' +
                '}';

        user.setId(testId);
        user.setFirstName(testFirstName);
        user.setLastName(testLastName);

        assertEquals(expected, user.toString());
    }
}
