package com.training.hackathon.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.training.hackathon.testutil.DummyEntityGenerator.generateDummyUser;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockPortfolioTest {
    private StockPortfolio stockPortfolio;
    private static final Long testId = 1L;
    private static final String testStock = "TEST";
    private static final Long testAmount = 35L;
    private static final User testUser = generateDummyUser(1L);
    private static final Long testIncreaseAmount = 12L;
    private static final Long testReduceAmount = 6L;

    @BeforeEach
    public void setup() {
        stockPortfolio = new StockPortfolio();
    }

    @Test
    public void testSetId() {
        stockPortfolio.setId(testId);
        assertEquals(testId, stockPortfolio.getId());
    }

    @Test
    public void testSetStock() {
        stockPortfolio.setStock(testStock);
        assertEquals(testStock, stockPortfolio.getStock());
    }

    @Test
    public void testSetAmount() {
        stockPortfolio.setAmount(testAmount);
        assertEquals(testAmount, stockPortfolio.getAmount());
    }

    @Test
    public void testSetUser() {
        stockPortfolio.setUser(testUser);
        assertEquals(testUser, stockPortfolio.getUser());
    }

    @Test
    public void testToString() {
        String expected = "StockPortfolio{" +
                "id=" + testId +
                ", stock='" + testStock + '\'' +
                ", amount=" + testAmount +
                ", user=" + testUser +
                '}';

        stockPortfolio.setId(testId);
        stockPortfolio.setStock(testStock);
        stockPortfolio.setAmount(testAmount);
        stockPortfolio.setUser(testUser);

        assertEquals(expected, stockPortfolio.toString());
    }

    @Test
    public void testIncreaseStockAmount() {
        Long expected = testAmount + testIncreaseAmount;

        stockPortfolio.setAmount(testAmount);
        stockPortfolio.increaseStockAmount(testIncreaseAmount);

        assertEquals(expected, stockPortfolio.getAmount());
    }

    @Test
    public void testReduceStockAmount() {
        Long expected = testAmount - testReduceAmount;

        stockPortfolio.setAmount(testAmount);
        stockPortfolio.reduceStockAmount(testReduceAmount);

        assertEquals(expected, stockPortfolio.getAmount());
    }
}
