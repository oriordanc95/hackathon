package com.training.hackathon.repository;

import com.training.hackathon.entity.StockPortfolio;
import com.training.hackathon.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StockPortfolioRepository extends JpaRepository<StockPortfolio, Long> {
    public List<StockPortfolio> findByUserId(Long userId);
    public Optional<StockPortfolio> findByStockAndUser(String stock, User user);
}
