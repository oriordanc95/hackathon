package com.training.hackathon.repository;

import com.training.hackathon.entity.StockTrade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface StockTradeRepository extends JpaRepository<StockTrade, Long> {
    List<StockTrade> findByStockTicker (String stockTicker);
    List<StockTrade> findByStatusCode (Long statusCode);
    List<StockTrade> findByUserId (Long userId);
}
