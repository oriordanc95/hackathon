package com.training.hackathon.testutil;

import com.training.hackathon.entity.StockPortfolio;
import com.training.hackathon.entity.StockTrade;
import com.training.hackathon.entity.User;

public class DummyEntityGenerator {
    public static StockTrade generateDummyStockTrade(Long id) {
        StockTrade stockTrade = new StockTrade();
        stockTrade.setId(id);
        stockTrade.setStockTicker("TEST");
        stockTrade.setPrice(25.5);
        stockTrade.setVolume(10L);
        stockTrade.setSide(StockTrade.Side.SELL);
        stockTrade.setStatusCode(0L);
        stockTrade.setUser(generateDummyUser(id));
        return stockTrade;
    }

    public static StockPortfolio generateDummyStockPortfolio(Long id) {
        StockPortfolio portfolio = new StockPortfolio();
        portfolio.setId(id);
        portfolio.setStock("TEST");
        portfolio.setAmount(20L);
        portfolio.setUser(generateDummyUser(id));
        return portfolio;
    }

    public static User generateDummyUser(Long id) {
        User user = new User();
        user.setId(id);
        user.setFirstName("Seto");
        user.setLastName("Kaiba");
        return user;
    }
}
