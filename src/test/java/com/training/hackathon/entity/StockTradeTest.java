package com.training.hackathon.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Date;

import static com.training.hackathon.testutil.DummyEntityGenerator.generateDummyUser;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockTradeTest {
    private static final String testStockTicker = "JUnit Test Stock Ticker";
    private static final StockTrade.Side testSide = StockTrade.Side.SELL;
    private static final Long testId = 0L;
    private static final Long testVolume = 1L;
    private static final Long testStatusCode = 2L;
    private static final Double testPrice = 500.00;
    private static final Long testTimeStamp = 946684800000L; // Midnight 1st Jan 2000 GMT+0000 in milliseconds Long
    private static final User testUser = generateDummyUser(1L);
    private StockTrade stockTrade;

    @BeforeEach
    public void setup() {
        stockTrade = new StockTrade();
    }

    @Test
    public void setIdTest() {
        stockTrade.setId(testId);

        assertEquals(testId, stockTrade.getId());
    }

    @Test
    public void setStockTickerTest() {
        stockTrade.setStockTicker(testStockTicker);

        assertEquals(testStockTicker, stockTrade.getStockTicker());
    }

    @Test
    public void setSideTest() {
        stockTrade.setSide(testSide);

        assertEquals(testSide, stockTrade.getSide());
    }

    @Test
    public void setVolumeTest() {
        stockTrade.setVolume(testVolume);

        assertEquals(testVolume, stockTrade.getVolume());
    }

    @Test
    public void setStatusCodeTest() {
        stockTrade.setStatusCode(testStatusCode);

        assertEquals(testStatusCode, stockTrade.getStatusCode());
    }

    @Test
    public void setPriceTest() {
        stockTrade.setPrice(testPrice);

        assertEquals(testPrice, stockTrade.getPrice());
    }

    @Test
    public void setCreatedTimestampTest() {
        stockTrade.setCreatedTimestamp(testTimeStamp);

        assertEquals(testTimeStamp, stockTrade.getCreatedTimestamp());
    }

    @Test
    public void toStringTest() {
        String expected = "StockTrade{" +
                "id=" + testId +
                ", stockTicker='" + testStockTicker + '\'' +
                ", price=" + testPrice +
                ", volume=" + testVolume +
                ", statusCode=" + testStatusCode +
                ", user=" + testUser +
                ", createdTimestamp=" + testTimeStamp +
                ", side=" + testSide +
                '}';

        stockTrade.setId(testId);
        stockTrade.setStockTicker(testStockTicker);
        stockTrade.setPrice(testPrice);
        stockTrade.setVolume(testVolume);
        stockTrade.setSide(testSide);
        stockTrade.setStatusCode(testStatusCode);
        stockTrade.setUser(testUser);
        stockTrade.setCreatedTimestamp(testTimeStamp);

        assertEquals(expected, stockTrade.toString());
    }
}
