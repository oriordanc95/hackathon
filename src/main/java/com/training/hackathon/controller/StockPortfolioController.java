package com.training.hackathon.controller;

import com.training.hackathon.entity.StockPortfolio;
import com.training.hackathon.service.StockPortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/portfolio")
public class StockPortfolioController {
    @Autowired
    StockPortfolioService service;

    @GetMapping
    public List<StockPortfolio> findAll() {
        return service.findAll();
    }

    @GetMapping("/{userId}")
    public List<StockPortfolio> findByUser(@PathVariable Long userId) {
        return service.findByUser(userId);
    }

    @PutMapping("/{tradeId}")
    public StockPortfolio changeStockAmount(@PathVariable Long tradeId) {
        return service.changeStockAmount(tradeId);
    }
}
