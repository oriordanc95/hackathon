package com.training.hackathon.service;

import com.training.hackathon.entity.StockTrade;
import com.training.hackathon.repository.StockTradeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class StockTradeServiceTest {

    @Mock
    private StockTradeRepository stockTradeRepository;

    StockTrade stockTrade;

    @BeforeEach
    public void setup() {

        this.stockTrade = new StockTrade();
        stockTrade.setStockTicker("AMZN");
        stockTrade.setSide(StockTrade.Side.BUY);
        stockTrade.setPrice(1000.0);
        stockTrade.setVolume(10L);
        stockTrade.setId(1L);
        stockTrade.setStatusCode(0L);


    }


    @Test
    public void saveTest(){

        when(stockTradeRepository.save(stockTrade)).thenReturn(stockTrade);
        StockTrade actual = stockTradeRepository.save(stockTrade);
        assertEquals(stockTrade,actual);
    }

    @Test
    public void findByIdTest(){

        when(stockTradeRepository.findById(1l)).thenReturn(Optional.of(stockTrade));
        Optional<StockTrade> actual = stockTradeRepository.findById(1l);

        Optional<StockTrade>  expected = Optional.ofNullable(stockTrade);

        assertEquals(expected,actual);

    }

    @Test
    public void findAllTest(){

        List<StockTrade> expected = new ArrayList<>();
        expected.add(stockTrade);

        when(stockTradeRepository.findAll()).thenReturn(expected);
        List<StockTrade> actual = stockTradeRepository.findAll();

        assertEquals(expected,actual);


    }
}
