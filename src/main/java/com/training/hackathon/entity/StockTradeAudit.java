package com.training.hackathon.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class StockTradeAudit {
    public enum Operation {
        INSERT,
        UPDATE,
        DELETE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long tradeId;

    @Column
    private Operation operation;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeDate;

    @Column
    private String changeDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTradeId() {
        return tradeId;
    }

    public void setTradeId(Long tradeId) {
        this.tradeId = tradeId;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeTime) {
        this.changeDate = changeTime;
    }

    public String getChangeDescription() {
        return changeDescription;
    }

    public void setChangeDescription(String changeDescription) {
        this.changeDescription = changeDescription;
    }

    @Override
    public String toString() {
        return "StockTradeAudit{" +
                "id=" + id +
                ", tradeId=" + tradeId +
                ", operation=" + operation +
                ", changeDate=" + changeDate +
                ", changeDescription='" + changeDescription + '\'' +
                '}';
    }
}
