package com.training.hackathon.controller;

import com.google.gson.Gson;
import com.training.hackathon.entity.User;
import com.training.hackathon.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.training.hackathon.testutil.DummyEntityGenerator.generateDummyUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

    @Mock
    private UserService serviceMock;

    @InjectMocks
    private UserController controller;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    @AutoConfigureTestDatabase
    public void testFindAll_EmptyList() throws Exception {
        mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));

        verify(serviceMock, times(1)).findAll();
    }

    @Test
    @AutoConfigureTestDatabase
    public void testFindAll_populatedList() throws Exception {
        List<User> expected = new ArrayList<>();
        expected.add(generateDummyUser(1L));

        when(serviceMock.findAll())
                .thenReturn(expected);

        MvcResult mvcResult = mockMvc.perform(get("/user"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();

        verify(serviceMock, times(1)).findAll();
        assertEquals(new Gson().toJson(expected), actual);
    }

    @Test
    @AutoConfigureTestDatabase
    public void testFindById_IdNotFound() throws Exception {
        when(serviceMock.findById(isA(Long.class)))
                .thenThrow(NoSuchElementException.class);

        mockMvc.perform(get("/user/1"))
                .andExpect(status().isNotFound());

        verify(serviceMock, times(1)).findById(isA(Long.class));
    }

    @Test
    @AutoConfigureTestDatabase
    public void testFindById_IdFound() throws Exception {
        User expected = generateDummyUser(1L);

        when(serviceMock.findById(isA(Long.class)))
                .thenReturn(expected);

        MvcResult mvcResult = mockMvc.perform(get("/user/1"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();

        verify(serviceMock, times(1)).findById(isA(Long.class));
        assertEquals(new Gson().toJson(expected), actual);
    }

    @Test
    @AutoConfigureTestDatabase
    public void testSave() throws Exception {
        User expected = generateDummyUser(1L);
        String expectedJson = new Gson().toJson(expected);

        when(serviceMock.save(isA(User.class)))
                .thenReturn(expected);

        MvcResult mvcResult =
                mockMvc.perform(post("/user")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedJson))
                        .andExpect(status().isCreated())
                        .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();

        verify(serviceMock, times(1)).save(isA(User.class));
        assertEquals(expectedJson, actual);
    }
}
