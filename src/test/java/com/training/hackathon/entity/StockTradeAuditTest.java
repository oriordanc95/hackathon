package com.training.hackathon.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockTradeAuditTest {

    private static final Long testId = 12L;
    private static final Long testTradeId = 3L;
    private static final StockTradeAudit.Operation testOperation = StockTradeAudit.Operation.UPDATE;
    private static final Date testChangeDate = new Date();
    private static final String testChangeDescription = "Status updated from 0 to 1";

    private StockTradeAudit stockTradeAudit;

    @BeforeEach
    public void setup() {
        stockTradeAudit = new StockTradeAudit();
    }

    @Test
    public void testSetId() {
        stockTradeAudit.setId(testId);

        assertEquals(testId, stockTradeAudit.getId());
    }

    @Test
    public void testSetTradeId() {
        stockTradeAudit.setTradeId(testTradeId);

        assertEquals(testTradeId, stockTradeAudit.getTradeId());
    }

    @Test
    public void testSetOperation() {
        stockTradeAudit.setOperation(testOperation);

        assertEquals(testOperation, stockTradeAudit.getOperation());
    }

    @Test
    public void testSetChangeDate() {
        stockTradeAudit.setChangeDate(testChangeDate);

        assertEquals(testChangeDate, stockTradeAudit.getChangeDate());
    }

    @Test
    public void testSetChangeDescription() {
        stockTradeAudit.setChangeDescription(testChangeDescription);

        assertEquals(testChangeDescription, stockTradeAudit.getChangeDescription());
    }

    @Test
    public void testToString() {
        String expected = "StockTradeAudit{" +
                "id=" + testId +
                ", tradeId=" + testTradeId +
                ", operation=" + testOperation +
                ", changeDate=" + testChangeDate +
                ", changeDescription='" + testChangeDescription + '\'' +
                '}';

        stockTradeAudit.setId(testId);
        stockTradeAudit.setTradeId(testTradeId);
        stockTradeAudit.setOperation(testOperation);
        stockTradeAudit.setChangeDate(testChangeDate);
        stockTradeAudit.setChangeDescription(testChangeDescription);

        assertEquals(expected, stockTradeAudit.toString());
    }
}
