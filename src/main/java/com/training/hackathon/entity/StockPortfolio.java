package com.training.hackathon.entity;

import javax.persistence.*;

@Entity
public class StockPortfolio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String stock;

    @Column
    private Long amount = 0L;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    public StockPortfolio() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "StockPortfolio{" +
                "id=" + id +
                ", stock='" + stock + '\'' +
                ", amount=" + amount +
                ", user=" + user +
                '}';
    }

    public void increaseStockAmount(Long amount) {
        this.amount += amount;
    }

    public void reduceStockAmount(Long amount) {
        this.amount -= amount;
    }
}
