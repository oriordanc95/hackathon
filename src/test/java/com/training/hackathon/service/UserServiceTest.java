package com.training.hackathon.service;

import com.training.hackathon.entity.User;
import com.training.hackathon.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static com.training.hackathon.testutil.DummyEntityGenerator.generateDummyUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository repositoryMock;

    @InjectMocks
    private UserService service;

    @Test
    public void testFindAll() {
        List<User> expected = new ArrayList<>();
        expected.add(generateDummyUser(1L));

        when(repositoryMock.findAll())
                .thenReturn(expected);

        assertEquals(expected, service.findAll());
        verify(repositoryMock, times(1)).findAll();
    }

    @Test
    public void testFindById_IdFound() {
        User expected = generateDummyUser(1L);

        when(repositoryMock.findById(isA(Long.class)))
                .thenReturn(java.util.Optional.of(expected));

        assertEquals(expected, service.findById(1L));
        verify(repositoryMock, times(1)).findById(isA(Long.class));
    }

    @Test
    public void testFindById_IdNotFound() {
        when(repositoryMock.findById(isA(Long.class)))
                .thenReturn(java.util.Optional.empty());

        assertThrows(NoSuchElementException.class, () -> service.findById(1L));
        verify(repositoryMock, times(1)).findById(isA(Long.class));
    }

    @Test
    public void testSave() {
        User expected = generateDummyUser(1L);

        when(repositoryMock.save(isA(User.class)))
                .thenReturn(expected);

        assertEquals(expected, service.save(expected));
    }
}
