package com.training.hackathon.controller;

import com.google.gson.Gson;
import com.training.hackathon.entity.StockPortfolio;
import com.training.hackathon.service.StockPortfolioService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static com.training.hackathon.testutil.DummyEntityGenerator.generateDummyStockPortfolio;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class StockPortfolioControllerTest {

    @Mock
    private StockPortfolioService serviceMock;

    @InjectMocks
    private StockPortfolioController controller;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testFindAll_EmptyList() throws Exception {
        mockMvc.perform(get("/portfolio"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));

        verify(serviceMock, times(1)).findAll();
    }

    @Test
    public void testFindAll_PopulatedList() throws Exception {
        List<StockPortfolio> expected = new ArrayList<>();
        expected.add(generateDummyStockPortfolio(1L));

        when(serviceMock.findAll())
                .thenReturn(expected);

        MvcResult mvcResult = mockMvc.perform(get("/portfolio"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();

        verify(serviceMock, times(1)).findAll();
        assertEquals(new Gson().toJson(expected), actual);
    }

    @Test
    public void testFindByUser_EmptyList() throws Exception {
        mockMvc.perform(get("/portfolio/1"))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));

        verify(serviceMock, times(1)).findByUser(isA(Long.class));
    }

    @Test
    public void testFindByUser_PopulatedList() throws Exception {
        List<StockPortfolio> expected = new ArrayList<>();
        expected.add(generateDummyStockPortfolio(1L));

        when(serviceMock.findByUser(isA(Long.class)))
                .thenReturn(expected);

        MvcResult mvcResult = mockMvc.perform(get("/portfolio/1"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();

        verify(serviceMock, times(1)).findByUser(isA(Long.class));
        assertEquals(new Gson().toJson(expected), actual);
    }

    @Test
    public void testChangeStockAmount() throws Exception {
        StockPortfolio expected = generateDummyStockPortfolio(1L);

        when(serviceMock.changeStockAmount(isA(Long.class)))
                .thenReturn(expected);

        MvcResult mvcResult = mockMvc.perform(put("/portfolio/1"))
                .andExpect(status().isOk())
                .andReturn();

        String actual = mvcResult.getResponse().getContentAsString();

        verify(serviceMock, times(1)).changeStockAmount(isA(Long.class));
        assertEquals(new Gson().toJson(expected), actual);
    }
}
