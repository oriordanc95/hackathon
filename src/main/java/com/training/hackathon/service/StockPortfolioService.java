package com.training.hackathon.service;

import com.training.hackathon.entity.StockPortfolio;
import com.training.hackathon.entity.StockTrade;
import com.training.hackathon.repository.StockPortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StockPortfolioService {
    @Autowired
    StockPortfolioRepository stockPortfolioRepository;

    @Autowired
    StockTradeService stockTradeService;

    public List<StockPortfolio> findAll() {
        return stockPortfolioRepository.findAll();
    }

    public List<StockPortfolio> findByUser(Long userId) {
        return stockPortfolioRepository.findByUserId(userId);
    }

    private StockPortfolio increaseStock(StockPortfolio portfolio, Long amount) {
        portfolio.increaseStockAmount(amount);
        return stockPortfolioRepository.save(portfolio);
    }

    private StockPortfolio reduceStock(StockPortfolio portfolio, Long amount) {
            portfolio.reduceStockAmount(amount);
            return stockPortfolioRepository.save(portfolio);
    }

    public StockPortfolio changeStockAmount(Long tradeId) {
        StockTrade trade = stockTradeService.findById(tradeId);
        StockPortfolio portfolio;

        try {
            portfolio = stockPortfolioRepository.findByStockAndUser(trade.getStockTicker(), trade.getUser()).get();
        } catch (NoSuchElementException e) {
            portfolio = new StockPortfolio();
            portfolio.setUser(trade.getUser());
            portfolio.setStock(trade.getStockTicker());
        }

        if (trade.getSide() == StockTrade.Side.BUY) {
            return increaseStock(portfolio, trade.getVolume());
        } else {
            return reduceStock(portfolio, trade.getVolume());
        }
    }
}
