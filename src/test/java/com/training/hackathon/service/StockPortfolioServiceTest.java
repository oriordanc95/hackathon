package com.training.hackathon.service;

import com.training.hackathon.entity.StockPortfolio;
import com.training.hackathon.repository.StockPortfolioRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.training.hackathon.testutil.DummyEntityGenerator.generateDummyStockPortfolio;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StockPortfolioServiceTest {

    @Mock
    private StockPortfolioRepository repositoryMock;

    @InjectMocks
    private StockPortfolioService service;

    @Test
    public void testFindAll() {
        List<StockPortfolio> expected = new ArrayList<>();
        expected.add(generateDummyStockPortfolio(1L));

        when(repositoryMock.findAll())
                .thenReturn(expected);

        assertEquals(expected, service.findAll());
        verify(repositoryMock, times(1)).findAll();
    }

    @Test
    public void testFindByUser() {
        List<StockPortfolio> expected = new ArrayList<>();
        expected.add(generateDummyStockPortfolio(1L));

        when(repositoryMock.findByUserId(isA(Long.class)))
                .thenReturn(expected);

        assertEquals(expected, service.findByUser(1L));
        verify(repositoryMock, times(1)).findByUserId(isA(Long.class));
    }
}
