package com.training.hackathon.service;

import com.training.hackathon.entity.StockTrade;
import com.training.hackathon.repository.StockTradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StockTradeService {

    @Autowired
    StockTradeRepository stockTradeRepository;

    public List<StockTrade> findAll(){
        return stockTradeRepository.findAll();

    }

    public List<StockTrade> findByUserId(Long userId) {
        return stockTradeRepository.findByUserId(userId);
    }

    public StockTrade findById(long id) {
        return stockTradeRepository.findById(id).get();
    }


    public List<StockTrade> findByStockTicker(String stockTicker) {
        return stockTradeRepository.findByStockTicker(stockTicker);
    }

    public List<StockTrade> findByStatusCode(Long statusCode) {
        return stockTradeRepository.findByStatusCode(statusCode);
    }

    public StockTrade save(StockTrade stocktrade) {
        stocktrade.setCreatedTimestamp(new Date().getTime());
        return stockTradeRepository.save(stocktrade);
    }


}

